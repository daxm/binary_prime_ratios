# Binary Prime Ratio
This project is built to look into the prime number distribution and ratio normalized against number ranges based on 
binary multiples.

The thought is that maybe prime numbers have a higher statistical chance of appearing in these ranges and we might can
predict a tighter range of numbers to test for primality.  (Or, conversely, we might find "dead zones" where it is
statistically unlikely a prime will appear.)

The program will generate 2 outputs for every pow(2,N) range.  The first will be a dictionary where the key will be the
prime number and the value will be its percentage placement in pow(2,N).  The second output will be a percentage of
primes found against the whole range (what percent of these numbers where prime).

