#!/usr/bin/python3

"""For now, no inputs.  Just modify the powers of 2 ranges you wish to collect data for and run the program."""

from sympy import isprime


# Range of powers of two to data collect.
pow2start = 10
pow2end = 15

print(f"Starting program.  Power of 2 range: 2^{pow2start} - 2^{pow2end}")

for powers in range(pow2start, pow2end + 1):
    prime_set = {}
    prime_counter = 0
    position_counter = 0
    current_range_start = pow(2,powers)
    current_range_end = pow(2,powers + 1)
    total_numbers = current_range_end - current_range_start
    for test_number in range(current_range_start, current_range_end):
        position_counter += 1
        if isprime(test_number):
            prime_counter += 1
            prime_set[test_number] = float(position_counter / total_numbers)
    print(f"----- Results for 2^{powers} -----")
    print(f"\t{prime_set}")
    print(f"\t{float(prime_counter / total_numbers)}")

print(f"Program completed.")